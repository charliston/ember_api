ActiveAdmin.register User do
  permit_params :id, :name, :username, :email, :password_digest, :created_at, :updated_at
end
