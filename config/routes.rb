Rails.application.routes.draw do
  resources :api_keys

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  mount API::Base, at: "/"
  mount GrapeSwaggerRails::Engine, at: "/doc"

  resources :users, except: [:new, :edit, :destroy]
  post 'session' => 'session#create'
end
